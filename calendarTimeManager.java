import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.text.*;
/**
 *
 * @author Alexio
 */
public class calendarTimeManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

    Date[] largestFreeBlock = calculateLongestTimeBlock(args[0]);
    System.out.println("Longest Block of Time Free to all Users");
    System.out.println("Start Time: " + largestFreeBlock[0]);
    System.out.println("End Time: " + largestFreeBlock[1]);
    }
    
    public static Date[] calculateLongestTimeBlock(String csvFileName){
        ArrayList<Date[]> dateList = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat tf = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date();
        String line = "";
        
        try{
            
            BufferedReader br = new BufferedReader(new FileReader(csvFileName));
            while((line = br.readLine()) != null){
                
                Date[] eventDatePair = new Date[2];
                String[] info = line.split(",");
                //Start date
                eventDatePair[0] = df.parse(info[1]);
                //End date
                eventDatePair[1] = df.parse(info[2]);
                
                String[] startTimeInfo = info[1].split(" ");
                String[] endTimeInfo = info[2].split(" ");
                long dateDiff = TimeUnit.DAYS.convert((eventDatePair[0].getTime() - currentDate.getTime()),TimeUnit.MILLISECONDS);
                
                //Within 7 days of the current date
                if(eventDatePair[0].after(currentDate) && dateDiff < 7){
                    //Between 8AM and 10PM
                    if(tf.parse(startTimeInfo[1]).after(tf.parse("08:00:00"))
                       && tf.parse(endTimeInfo[1]).before(tf.parse("22:00:00"))){
                        dateList.add(eventDatePair);
                    }
                }
            }
            
            //sort the list based on start date (Date[0])
            Collections.sort(dateList,new Comparator<Date[]>() {
                public int compare(Date[] dates, Date[] otherDates) {
                    return dates[0].compareTo(otherDates[0]);
                }
            });
            
            Date[] longestFreeBlockTimes = new Date[2];
            long longestFreeBlock = 0;
            for(int i = 0; i < (dateList.size() - 1); i++){
                Date[] currentPair = dateList.get(i);
                Date[] nextPair = dateList.get(i+1);
              
                if(currentPair[1].getDay() == nextPair[0].getDay()){
                    long timeDiff = (nextPair[0]).getTime() - (currentPair[1]).getTime();
                
                    if(timeDiff >= 0 && timeDiff > longestFreeBlock){
                        longestFreeBlock = timeDiff;
                        longestFreeBlockTimes[0] = currentPair[1];
                        longestFreeBlockTimes[1] = nextPair[0];
                    }
                }
            }
            
            return longestFreeBlockTimes;
            
        }catch(Exception e){
            System.out.println("Error: " + e);
            return null;
        }
    }
}
